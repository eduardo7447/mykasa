package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Mikasa {
	
	private double x;
	private double y;
	private double alto;
	private double ancho;
	private double angulo;
	private double velocidad;
	private boolean disparando;
	private boolean mikasaModoKyojin;
	private double escalaImg;
	private Image mikasaMoviendose;
	private Image mikasaDisparando;
	private Image mikasaKyojin;

	public Mikasa(int x, int y, double velocidad) {
		this.x = x;
		this.y = y;
		this.velocidad = velocidad;
		this.ancho = 50;
		this.alto = 50;
		this.angulo = 0;
		this.escalaImg = .1;
		this.disparando = false;
		this.mikasaModoKyojin = false;
		this.mikasaMoviendose = Herramientas.cargarImagen("mykasa.png");
		this.mikasaDisparando = Herramientas.cargarImagen("mykasaDisparando.png");
		this.mikasaKyojin = Herramientas.cargarImagen("mikasaKyojin.png");
	}

	public void dibujarse(Entorno entorno) {
		//dibuja a mikasa disparando cuando sea true
		if (this.disparando) {
			entorno.dibujarImagen(this.mikasaDisparando, this.x, this.y, this.angulo, this.escalaImg);
		} else if (this.mikasaModoKyojin) {
			//dibuja mikasa transformada si es true
			entorno.dibujarImagen(this.mikasaKyojin, this.x, this.y, this.angulo, 1.5);
		} else {
			//dibuja en estado original
			entorno.dibujarImagen(this.mikasaMoviendose, this.x, this.y, this.angulo, this.escalaImg);
		}
	}

	public void girarDerecha() {
		this.angulo += 0.1;
	}

	public void girarIzquierda() {
		this.angulo -= 0.1;
	}

	public void moverseAdelante() {
		//utiliza las posiciones de X e Y para moverse segun el angulo en la que se posiciona
		this.x += Math.cos(this.angulo) * this.velocidad;
		this.y += Math.sin(this.angulo) * this.velocidad;
	}

	public Proyectil disparar() {
		//instancia un nuevo proyectil
		return new Proyectil(this.x, this.y, this.angulo, this.velocidad + 2.5);
	}

	public boolean chocasteElBordeDerecho(Entorno entorno) {
		//devuelve si mikasa sobrepasa el ancho de la pantalla
		return this.x > entorno.ancho() - this.ancho;
	}

	public boolean chocasteElBordeIzquierdo(Entorno entorno) {
		//devuelve si mikasa sobrepasa el ancho de la pantalla izquierda
		return this.x < ancho;
	}

	public boolean chocasteElBordeSuperior(Entorno entorno) {
		//devuelve si mikasa sobrepasa el alto del entorno
		return y < alto;
	}

	public boolean chocasteElBordeInferior(Entorno entorno) {
		//devuelve si mikasa sobrepasa el alto del borde inferior 
		return y > entorno.alto() - alto;
	}

	public void detenerse(Entorno entorno) {
		if (this.chocasteElBordeDerecho(entorno)) {
			this.x = entorno.ancho() - ancho;
		}
		if (this.chocasteElBordeIzquierdo(entorno)) {
			this.x = ancho;
		}
		if (this.chocasteElBordeSuperior(entorno)) {
			this.y = alto;
		}
		if (this.chocasteElBordeInferior(entorno)) {
			this.y = entorno.alto() - alto;
		}
	}

	public boolean chocasteCon(KiojinNoKessei suero) {
		return Math.sqrt(Math.pow(Math.abs(this.x - suero.getX()), 2) +
			Math.pow(Math.abs(this.y - suero.getY()), 2)) < suero.getTamaño();
	}

	// duda con chocaste
	public boolean chocasteCon(Obstaculo o) {
		//
		double catetoX = this.x - o.getX();
		double catetoY = this.y - o.getY();
		if (catetoX < 0) {
			catetoX *= -1;
		}
		if (catetoY < 0) {
			catetoY *= -1;
		}
		//devuelve si la posicion en X es menor a la posicion del obstaculo
		return Math.sqrt(Math.pow(catetoX, 2) + Math.pow(catetoY, 2)) < 80;
	}

	public boolean chocasteCon(Kyojin j) {
		//devuelve si la x de la posicion es menor a la distancia del kyojin
		return Math.sqrt(Math.pow(Math.abs(this.x - j.getX()), 2) + Math.pow(Math.abs(this.y - j.getY()), 2)) < 80;
	}

	public void ponerEnModoAtacaque() { // ponerEnModoDisparo()
		this.disparando = true;
	}

	public void sacarModoAtaque() { // ponerEnModoDeNoDisparo()
		this.disparando = false;
	}

	public void tranformarEnKyojin() {
		this.mikasaModoKyojin = true;
	}

	public void noTansformadaEnKyojin() {
		this.mikasaModoKyojin = false;
	}

	public void cambiaDireccion() {
		this.angulo += Math.PI / 4;
	}

	// getters
	public boolean transformadaEnKyojin() {
		return mikasaModoKyojin;
	}

	public double getX() {
		return x;
	}

	public double getAlto() {
		return alto;
	}

	public double getAncho() {
		return ancho;
	}

	public double getY() {
		return y;
	}
}