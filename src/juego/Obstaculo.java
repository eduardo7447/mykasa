package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Obstaculo {
	
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private double angulo;
	private double escalaImg;
	private Image img;

	// constructor
	public Obstaculo(double x, double y) {
		this.x = x;
		this.y = y;
		this.ancho = 10;
		this.alto = 10;
		this.angulo =0;
		this.escalaImg = 0.1;
		this.img = Herramientas.cargarImagen("obstaculo2.png");
	}

	// dibuja el obstaculo
	public void dibujarse(Entorno entorno) {
		entorno.dibujarImagen(this.img, this.x, this.y, this.angulo, this.escalaImg);
	}
	public void girarDerecha() {
		this.angulo += 0.01;
	}
	public void girarIzquierda() {
		this.angulo -= 0.01;
	}
	// getters
	public double getAlto() {
		return alto;
	}

	public double getAncho() {
		return ancho;
	}

	
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
}