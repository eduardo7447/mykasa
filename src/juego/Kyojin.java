package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Kyojin {
	
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private double velocidad;
	private double angulo;
	private Image img;

	public Kyojin(double X, double Y, double velocidad) {
		this.x = X;
		this.y = Y;
		this.alto = 50;
		this.ancho = 50;
		this.velocidad = velocidad;
		this.angulo = -Math.PI / 4;
		this.img = Herramientas.cargarImagen("kyojin(abajo).png");
	}

	public void dibujarse(Entorno e) {
		e.dibujarImagen(this.img, this.x, this.y, 0, 1.5);
	}

	public void moverse() {
		this.x += Math.cos(this.angulo) * this.velocidad;
		this.y += Math.sin(this.angulo) * this.velocidad;
	}

	public void cambiarDireccion() {
		angulo += Math.PI / 4;
	}

	public boolean chocasteConElBorde(Entorno entorno) {
		//verifica la posicion de ambos y con ello obtiene la distancia
		return this.x + this.ancho > entorno.ancho() || this.x - this.ancho < 0 || this.y < this.alto ||
			this.y + this.alto / 2 > entorno.alto();
	}

	public boolean chocasteCon(Kyojin j) {
		//verifica la posicion de ambos y con ello obtiene la distancia
		return (Math.sqrt(Math.pow(Math.abs(this.x - j.getX()), 2) + Math.pow(Math.abs(this.y - j.getY()), 2))) < 83;
	}

	public boolean chocasteConObstaculo(Obstaculo o) {
		//verifica la posicion de ambos y con ello obtiene la distancia
		return (Math.sqrt(Math.pow(Math.abs(this.x - o.getX()), 2) + Math.pow(Math.abs(this.y - o.getY()), 2))) < 85;
	}

	public void perseguir(Mikasa mikasa) {
		//devuelve el angulo de avance hacia mikasa
		this.angulo = Math.atan2(mikasa.getY() - this.y, mikasa.getX() - this.x);
	}

	public boolean chocaCon(Kyojin[] vivos) {
		//devuelve un boleeano que confirma si chocaste con algun Kyojin
		return cuantosChocaste(vivos) != 0;
	}

	//devuelve la cantidad de Kyojines que chocaste
	public int cuantosChocaste(Kyojin[] vi) {
		int cont = 0;
		for (Kyojin k: vi) {
			if (this.chocasteCon(k)) {
				cont++;
			}
		}
		return cont;
	}
	// getters

	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getVelocidad() {
		return velocidad;
	}
}