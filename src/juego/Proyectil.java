package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Proyectil {
	private double x;
	private double y;
	private double angulo;
	private double velocidad;
	private double alto;
	private double ancho;
	private double escala;
	private Image img;

	public Proyectil(double x, double y, double angulo, double velocidad) {
		this.x = x;
		this.y = y;
		this.alto = 10;
		this.ancho = 20;
		this.velocidad = velocidad;
		this.escala = 0.1;
		this.angulo = angulo;
		this.img = Herramientas.cargarImagen("poder.png");
	}

	public boolean ChocasteConKyojin(Kyojin k) {
		return (Math.sqrt(Math.pow(Math.abs(this.x - k.getX()), 2) + Math.pow(Math.abs(this.y - k.getY()), 2))) < 50;
	}

	// se dibuja el proyectil
	public void dibujarse(Entorno entorno) {
		entorno.dibujarImagen(this.img, this.x, this.y, angulo, escala);
	}

	// movimiento del proyectil
	public void moverse() {
		this.x += Math.cos(this.angulo) * this.velocidad;
		this.y += Math.sin(this.angulo) * this.velocidad;
	}

	// devuelve el x
	public boolean chocasteElBorde(Entorno entorno) {
		return this.x > entorno.ancho() - this.ancho / 2 || this.x < -this.ancho / 2 ||
			this.y > entorno.alto() - this.alto || this.y < -this.alto;
	}

	// choque con el obstaculo
	public boolean chocasteCon(Obstaculo o) {
		return (this.x + this.ancho > o.getX() - o.getAncho() * 2 && this.x - this.ancho < o.getX() + o.getAncho() * 2) &&
			(this.y + this.ancho > o.getY() - o.getAlto() * 2 &&
				this.y - this.ancho < o.getY() + o.getAlto() * 2);
	}

	// getters
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAlto() {
		return alto;
	}

	public double getAncho() {
		return ancho;
	}
}