package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class KiojinNoKessei {
	
	private double x;
	private double y;
	private int tamaño;
	private double escalaImg;
	private Image img;

	public KiojinNoKessei(double X, double Y) {
		this.x = X;
		this.y = Y;
		this.escalaImg = 0.1;
		this.tamaño = 50;
		this.img = Herramientas.cargarImagen("suero.png");
	}

	// se dibuja en el entorno
	public void dibujarse(Entorno entorno) {
		entorno.dibujarImagen(this.img, this.x, this.y, this.escalaImg);
	}

	// getters
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public int getTamaño() {
		return tamaño;
	}

	public double getScale() {
		return escalaImg;
	}
}