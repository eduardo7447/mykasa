package juego;
import java.awt.Color;
import java.awt.Image;
import entorno.*;
import java.io.*;
import javax.sound.sampled.*;

public class Juego extends InterfaceJuego {
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Image fondo;
	private Image gameOver;
	private Image winner;
	private Mikasa mikasa;
	private Kyojin[] kyojines;
	private Kyojin[] kyojinesVivos;
	private Kyojin kyojinAux;
	private Obstaculo[] obstaculos;
	private Proyectil proyectil;
	private String mensajeSalida;
	private String mensajeEliminados;
	private boolean perdiste;
	private boolean ganaste;
	private boolean reiniciaJuego;
	private int cantidadDeKyojinesVivos;
	private int tiempoDeSuero;
	private int tiempoTotal;
	private int cantidadDeKyojinesMuertos;
	private KiojinNoKessei suero;

	public Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Attack on Titan, Final Season", 800, 600);
		this.iniciarElementosDelJuego();
		// Inicia el juego!
		this.entorno.iniciar();
	}

	public void ReproducirSonido(String sonido) {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(sonido).getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
			System.out.println("Error al reproducir el sonido.");
		}
	}
	
	//metodo que invoca a iniciar los elementos del juego
	private void iniciarElementosDelJuego() {
		this.ReproducirSonido("src/sonidoGanador.wav");
		this.fondo = Herramientas.cargarImagen("espacio2.jpg");
		this.gameOver = Herramientas.cargarImagen("gameover.jpg");
		this.winner = Herramientas.cargarImagen("winner.jpg");
		this.mikasa = new Mikasa(entorno.ancho() / 2, entorno.alto() / 2, 1.5);
		this.kyojines = new Kyojin[4];
		this.obstaculos = new Obstaculo[4];
		this.mensajeSalida = "S = salir";
		this.mensajeEliminados = "Kyojines muertos: ";
		this.tiempoDeSuero = 0;
		this.tiempoTotal = 0;
		this.cantidadDeKyojinesMuertos = 0;
		this.perdiste = false;
		this.ganaste = false;
		this.cantidadDeKyojinesVivos = this.kyojines.length;
		this.reiniciaJuego = false;
		double rKiojinX = 100;
		double rKiojinY = entorno.alto() - 45;
		double xObtaculo = 200;
		double yObtaculo = 150;
		double velocidadKyojin = .5;


		//crea todos los obstaculos
		for (int i = 0; i < this.obstaculos.length; i++) {
			if (i <= 1) {
				this.obstaculos[i] = new Obstaculo(xObtaculo, yObtaculo);
				xObtaculo += 400;
			}
			//crea los obstaculos en otra posicion de Y
			if (i > 1) {
				yObtaculo = 450;
				this.obstaculos[i] = new Obstaculo(xObtaculo - 800, yObtaculo);
				xObtaculo += 400;
			}
		}

		//Crea cada kyojines
		for (int i = 0; i < this.kyojines.length; i++) {
			if (i <= 1) {
				rKiojinY = 50;
				this.kyojines[i] = new Kyojin(rKiojinX, rKiojinY, velocidadKyojin);
				rKiojinX += Math.random() * (600 - 150) + 150;
				//creamos el kyiojin a otra posicion de X para que no se superpongan al inicio
				if (i == 1) {
					rKiojinX = 100;
				}
			}
			//si un kyojin en otra posicion en diferente X con respecto al primero
			if (i > 1) {
				rKiojinY = entorno.alto() - 45;
				this.kyojines[i] = new Kyojin(rKiojinX, rKiojinY, velocidadKyojin);
				rKiojinX += Math.random() * (600 - 150) + 150;
			}

		}
	}
	public void tick() {
		//pantalla que muestra si perdiste
		if (perdiste) {
			// borrar los kiojines
			this.mikasa = null;
			for (int ki = 0; ki < this.kyojines.length; ki++) {
				kyojines[ki] = null;
			}
			// borrar obstaculos
			for (int obs = 0; obs < this.obstaculos.length; obs++) {
				obstaculos[obs] = null;
			}
			// se dibuja el mensaje de partida perdida
			this.entorno.dibujarImagen(gameOver, entorno.ancho() / 2, entorno.alto() / 2, 0, 0.3);

			this.entorno.cambiarFont("Goudy Stout", 60, Color.RED);
			// dibujar en la mitad de la pantalla
			this.entorno.escribirTexto("GAME OVER", entorno.ancho() / 5 - "GAME OVER".length() * 10,
				entorno.alto() / 4);
			// escribir cantidad de kiojines muertos
			this.entorno.cambiarFont("Goudy Stout", 25, Color.yellow);
			this.entorno.escribirTexto(this.mensajeEliminados + this.cantidadDeKyojinesMuertos,
				entorno.ancho() / 2.5 - this.mensajeEliminados.length() * 10, entorno.alto() / 1.2);
			// tiempo de juego
			this.entorno.cambiarFont("Goudy Stout", 20, Color.white);
			this.entorno.escribirTexto("Tiempo de juego: " + this.tiempoTotal / 60,
				entorno.ancho() / 2.15 - "Tiempo de juego: ".length() * 10, entorno.alto() / 1.1);
			// boton salir
			this.entorno.cambiarFont("Goudy Stout", 20, Color.yellow);
			this.entorno.escribirTexto(mensajeSalida, mensajeSalida.length(), 20);
			this.entorno.escribirTexto("R = reiniciar", entorno.ancho() - "R = reiniciar".length() * 22, 20);

			if (entorno.sePresiono('s')) {
				System.exit(0);
			}
			if (entorno.sePresiono('r')) {
				this.perdiste = false;
				this.reiniciaJuego = true;
			}
			return;
		}
		//
		if (ganaste) {
			// mostrar pantalla de ganaste
			// borrar obstaculos
			for (int obs = 0; obs < this.obstaculos.length; obs++) {
				obstaculos[obs] = null;
			}
			this.entorno.dibujarImagen(winner, entorno.ancho() / 2, entorno.alto() / 2, 0, 0.15);
			//sonido

			this.entorno.cambiarFont("Goudy Stout", 60, Color.RED);
			// dibujar en la mitad de la pantalla
			this.entorno.escribirTexto("WINNER", entorno.ancho() / 3.5 - "WINNER".length() * 10, entorno.alto() / 1.5);
			// boton cerrar pantalla
			this.entorno.cambiarFont("Goudy Stout", 20, Color.yellow);
			this.entorno.escribirTexto(mensajeSalida, mensajeSalida.length(), 20);
			// boton reinicir juego
			this.entorno.escribirTexto("R = reiniciar", entorno.ancho() - "R = reiniciar".length() * 22, 20);
			// escribir cantidad de kiojines muertos
			this.entorno.cambiarFont("Goudy Stout", 30, Color.yellow);
			this.entorno.escribirTexto(this.mensajeEliminados + this.cantidadDeKyojinesMuertos,
				entorno.ancho() / 5 - "WINNER".length() * 10, entorno.alto() / 1.2);
			// tiempo de juego
			this.entorno.cambiarFont("Goudy Stout", 20, Color.white);
			this.entorno.escribirTexto("Tiempo de juego: " + this.tiempoTotal / 60,
				entorno.ancho() / 3 - "WINNER".length() * 10, entorno.alto() / 1.1);
			if (entorno.sePresiono('s')) {
				System.exit(0);
			}
			if (entorno.sePresiono('r')) {
				this.ganaste = false;
				this.reiniciaJuego = true;
				// this.iniciarElementosDelJuego();
			}
			return;
		}

		if (reiniciaJuego) {
			iniciarElementosDelJuego();
		}
		
		// acá va el código normal de jugar
		this.tiempoDeSuero++;
		this.tiempoTotal++;
		this.entorno.dibujarImagen(fondo, 0, 0, 0, 0.6);
		this.mikasa.dibujarse(entorno);
		this.entorno.cambiarFont("Castellar", 20, Color.CYAN);
		this.entorno.escribirTexto(this.mensajeSalida, this.mensajeSalida.length(), 20);
		this.entorno.cambiarFont("Castellar", 20, Color.CYAN);
		this.entorno.escribirTexto("TIEMPO: " + "" + this.tiempoTotal / 60, 10, entorno.alto() - 5);
		this.entorno.cambiarFont("Castellar", 20, Color.CYAN);
		this.entorno.escribirTexto(this.mensajeEliminados + "" + this.cantidadDeKyojinesMuertos,
			entorno.ancho() - this.mensajeEliminados.length() * 14, entorno.alto() - 5);
		// dibujar obstaculos
		for (Obstaculo o: obstaculos) {
			// si el proyectil toca un obstaculo se elimina el proyectil y mikasa sale del modo ataque
			if (proyectil != null && proyectil.chocasteCon(o)) {
				proyectil = null;
				o.girarIzquierda(); // cuidado!
				mikasa.sacarModoAtaque();
			}
		}
		for(int i =0;i< obstaculos.length; i++){
			if(i%2==0){
				obstaculos[i].girarIzquierda();
			}else{
				obstaculos[i].girarDerecha();

			}
		}
		// dibujar kyojines y darle movimiento 
		for (Kyojin k: kyojines) {
			if (k != null) {
				k.dibujarse(entorno);
				// los kyojines se mueven segun el tiepo transcurrido en pantalla
				if ((tiempoDeSuero / 60) % 2 == 0) {
					k.moverse();
				}
				if ((tiempoDeSuero / 60) % 2 != 0) {
					k.perseguir(mikasa);
				}
				// si mikasa choca con un kyojin y no esta transformada en  kyojin ella muere y se termina la partida
				if (this.mikasa.chocasteCon(k) && !this.mikasa.transformadaEnKyojin()) {
					this.perdiste = true;
				}
				// si un kiojin choca con borde cambia de direccion
				if (k.chocasteConElBorde(entorno) && k != null) {
					k.cambiarDireccion();
				}
				for (Obstaculo o: obstaculos) {
					o.dibujarse(entorno);
					//si mikasa o un kyojin choca con un obstaculo estos cambian de direccion
					if (k.chocasteConObstaculo(o)) {
						k.cambiarDireccion();
					}
					if (mikasa.chocasteCon(o) && entorno.estaPresionada(entorno.TECLA_ARRIBA)) {
						mikasa.cambiaDireccion();
					}
				}
				for (Kyojin j: kyojines) {
					// si un kyojin choca con otro kyojin cambia de direccion
					if (k != null && j != null) {
						if (!k.equals(j)) {
							if (k.chocasteCon(j)) {
								k.cambiarDireccion();
								j.cambiarDireccion();
							}
						}
					}
				}
			}
		}
		//kyojin morir y renacer
		for (int ki = 0; ki < this.kyojines.length; ki++) {
			// si choca el proyectil con el kyojin ambos se ponen en null
			// disminuye la cantidad de kyojines vivos en el juego y aumentan la cantidad de muertos
			if (kyojines[ki] != null && proyectil != null && proyectil.ChocasteConKyojin(kyojines[ki])) {
				proyectil = null;
				kyojines[ki] = null;
				mikasa.sacarModoAtaque();
				this.cantidadDeKyojinesVivos--;
				this.cantidadDeKyojinesMuertos++;
			}
			if (this.kyojines[ki] != null && this.mikasa.chocasteCon(kyojines[ki]) &&
				this.mikasa.transformadaEnKyojin()) {
				kyojines[ki] = null;
				mikasa.noTansformadaEnKyojin();
				this.tiempoDeSuero = 0;
				this.cantidadDeKyojinesVivos--;
				this.cantidadDeKyojinesMuertos++;
			}
			// si los kiojines vivos son meroes a 4 se crea un array de kiojines vivos
			if (this.cantidadDeKyojinesVivos < 4) {
				int p = 0;
				this.kyojinesVivos = new Kyojin[this.cantidadDeKyojinesVivos];
				for (int k3 = 0; k3 < this.kyojines.length; k3++) {
					// se genera una copia de los kiojines vivos
					if (kyojines[k3] != null) {
						this.kyojinesVivos[p++] = new Kyojin(kyojines[k3].getX(), kyojines[k3].getY(),
							kyojines[k3].getVelocidad());
					}
				}
			}
			// si hay kiojines muertos crea posicion aleatoria y regenera el kiojin
			if (this.tiempoTotal / 60 % 15 == 0 && this.kyojines[ki] == null) {
				double rx = Math.random() * (700 - 100) + 85;
				double ry = (entorno.alto() - 45);
				double velocidad = .5;
				// posicion de kyojines arriba o abajo
				double a = ry, b = 50;
				if (this.tiempoTotal % 2 == 0) {
					ry = b;
				}
				if (this.tiempoTotal % 2 != 0) {
					ry = a;
				}
				this.kyojinAux = new Kyojin(rx, ry, velocidad);
				// si el kiojin auxiliar no toca con otro kiojin vivo y mikasa no choca con el
				// kiojin auxiliar se crea el kiojin
				if (!kyojinAux.chocaCon(kyojinesVivos) && !mikasa.chocasteCon(kyojinAux)) {
					this.kyojines[ki] = new Kyojin(rx, ry, velocidad);
					this.kyojinAux = null;
					this.cantidadDeKyojinesVivos++;
				}
			}
		}
		if (this.tiempoDeSuero / 60 > 3 && this.tiempoDeSuero / 60 < 10 && this.suero == null &&
			!this.mikasa.transformadaEnKyojin()) {
			this.suero = new KiojinNoKessei(Math.random() * (700 - 100) + 85, this.entorno.ancho() / 3);
		}
		if (this.suero != null && !mikasa.chocasteCon(this.suero)) {
			this.suero.dibujarse(entorno);
		}
		if (this.suero != null && this.mikasa.chocasteCon(this.suero)) {
			this.mikasa.tranformarEnKyojin();
		}
		if (this.mikasa.transformadaEnKyojin()) {
			this.suero = null;
		}
		if (this.mikasa.transformadaEnKyojin() && this.tiempoDeSuero / 60 > 10) {
			this.mikasa.noTansformadaEnKyojin();
			this.tiempoDeSuero = 0;
		}
		// si se preciona la tecla de flecha arriba mikasa gira a la derecha
		if (entorno.estaPresionada(entorno.TECLA_DERECHA)) {
			mikasa.girarDerecha();
		}
		// si se preciona la tecla de flecha arriba mikasa gira a la izquierda
		if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
			mikasa.girarIzquierda();
		}
		// si se preciona la tecla de flecha arriba mikasa avanza hacia adelante
		if (entorno.estaPresionada(entorno.TECLA_ARRIBA)) {
			// si se generan coliciones con los bordes se detiene
			if (entorno.estaPresionada(entorno.TECLA_ARRIBA) && mikasa.chocasteElBordeDerecho(entorno) ||
				entorno.estaPresionada(entorno.TECLA_ARRIBA) && mikasa.chocasteElBordeIzquierdo(entorno) ||
				entorno.estaPresionada(entorno.TECLA_ARRIBA) && mikasa.chocasteElBordeSuperior(entorno) ||
				entorno.estaPresionada(entorno.TECLA_ARRIBA) && mikasa.chocasteElBordeInferior(entorno)) {
				mikasa.detenerse(entorno);
			}
			mikasa.moverseAdelante();
		}
		if (this.mikasa != null && this.cantidadDeKyojinesVivos == 0) {
			this.ganaste = true;
		}
		// si se preciona la barra de espacio mikasa lanza un proyectil
		if (entorno.estaPresionada(entorno.TECLA_ESPACIO)) {
			if (proyectil == null) {
				proyectil = mikasa.disparar();
			}
		}
		// si el proyectil no es nulo se dibuja y se mueve
		if (proyectil != null) {
			proyectil.dibujarse(entorno);
			proyectil.moverse();
			mikasa.ponerEnModoAtacaque();
		}
		// si el proyectil no es nulo y choca con el borde se elimina
		if (proyectil != null && proyectil.chocasteElBorde(entorno)) {
			mikasa.sacarModoAtaque();
			proyectil = null;
		}
		// si precionamos la tecla x se cierra el juego
		if (entorno.estaPresionada('s')) {
			System.exit(0);
		}
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}
}